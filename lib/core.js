// core.js
// Contains classes and there constructors, attributes, getters and setters.
	LIB = new Object();

	LIB.getTime = function() { // return the current time : day/month/fullyear, hours:minutes:seconds. ex: 21/12/2012, 21:12:21
		var date = new Date();
		return date.getDate() + '/' + date.getMonth() + '/' + date.getFullYear() + ', ' + date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
	};
	
	LIB.getComputer = function() {
		return settings.computer;
	};
	
	LIB.firstLetterInMaj = function (word) { //Return the word with the first letter uppered and rest lowered, ex: jEan CHArles -> Jean Charles
		word = word[0].toUpperCase() + word.substr(1, word.length - 1).toLowerCase();
		for (var i = 0; i < word.length; i++) {
			if (word[i] = ' ') {
				word[i + 1].toUpperCase();
			}
		};
	
		return word
	};
	
	LIB.pushSkill = function(Object, level) {
		var Skill = Object;
		Skill.setLevel(level); 
		return Skill;
	};

var BANANA = function() {

	
	this.compileJade = function(Attributes) {
	
		if (Attributes.type == 'file') {
			var render = jade.compileFile(Attributes.path);
			return render(Attributes.locals).toString();
		}

		if (Attributes.type == 'string') {
			var render = jade.compile(Attributes.text);
			return render(Attributes.locals).toString();
		}
	};
	
	this.engine = function(Attributes) {
		/*	Attributes
				- Headers 	Array 	(Array which contains all the Html Headers needs)
				- Features 	Array 	(Array which contains all the features needed for the Headers)
				- Html 		Array   (Array which contains all the Html and Javascript to put after the Headers)
				- Footers 	Array 	(Array which contains all the Html Footers needs)
		*/
	};

	this.App = function(Attributes) {
	
		/*
			title: 		String 	(The title of the app)
			Features:   Array 	(An array containing all the features to render by the engine)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.title;
		this.header;
		this.Features;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setTitle = function(title) {
			this.title = title + '';
		};
	
		this.setHeader = function(header) {
			this.header = header;
		};
	
		this.setFeatures = function(Features) {
			this.Features = Features + '';
		};
		//Setting attributes by setters functions
	
		this.setTitle(Attributes.title);
		this.setHeader(Attributes.header);
		this.setFeatures(Attributes.Features);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getTitle = function() {
			return this.title;
		};
	
		this.getHeader = function() {
			return this.header;
		};
	
		this.getFeatures = function() {
			return this.Features;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class Feature : //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.Feature = function(Attributes) {
	
		/*
			id: 				Int 		(The ID of the feature)
			title: 			String 	(The title of the feature)
			description String 	(A quick description of the feature)
			file: 			String/File (the name of the feature's file)
			Views: 			Array 	(Array containing all the view files)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.title;
		this.description;
		this.file;
		this.Views;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a Feature object';
			}
		};
	
		this.setTitle = function(title) {
			this.title = title + '';
		};
	
		this.setDescription = function(description) {
			this.description = description + '';
		};
	
		this.setFile = function(File) {
			this.File = File;
		};
	
		this.setViews = function(Views) {
			this.Views = Views;
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setTitle(Attributes.title);
		this.setDescription(Attributes.description);
		this.setFile(Attributes.File);
		this.setViews(Attributes.Views);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getTitle = function() {
			return this.title;
		};
	
		this.getDescription = function() {
			return this.description;
		};
	
		this.getFile = function() {
			return this.File;
		};
	
		this.getViews = function() {
			return this.Views;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class User : /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.User = function(Attributes)  {
	
		/*
			id: 		Int 	(ID of the user)
			username: 	String 	(Username of the user)
			password: 	String/Sha256 	(A string which contains encrypted user's password)
			name: 		String 	(Name of the user)
			lastname: 	String 	(Lastname of the user)
			Type: 		Object 	(Describe the user's type ex: Professional, Student...)
			picture:  	String 	(Url of the profil picture)
			age:  		Int		(Age of the user)
			Skills: 	Array 	(Array to list user's Skills)
			Status: 	Object 	(Describe the status of the user ex: Subscribed...)
			Timetable:  Object	(Object which contains statistics to know when the user is in the lab)
			Creations: 	Object 	(Object which contains basics informations about the user's Creations)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.username;
		this.password;
		this.name;
		this.lastname;
		this.type;
		this.picture;
		this.age;
		this.Skills;
		this.status;
		this.Timetable;
		this.Creations;
	
		this.loginTime 	= LIB.getTime();			//Initialized attributes
		this.computerNumber = LIB.getComputer();
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a User object';
			}
		};
	
		this.setUsername = function(username) {
			this.username = username + ''; //Converting into a string
		};
	
		this.setPassword = function(password) {
			var password = password + ''; //Converting into a string
			if (password.length == 64) { //Checking the length of the password according to sha256 string length
				this.password = password;
			} else {
				throw 'Error in this.setPassword(): The password is not in SHA256, it must be 64 caracters long. Current: ' + password.length;
			}
		};
	
		this.setName = function(name) {
			this.name = name + '';
		};
	
		this.setLastname = function(lastname) {
			this.lastname = lastname + '';
		};
	
		this.setType = function(Type) {
			if(typeof Type == "object") {
				this.Type = Type;
			} else {
				throw 'The type of the Type is not "Object". Current: ' + typeof Type + 'Into a User object';
			}
		};
	
		this.setPicture = function(picture) {
			this.picture = picture + '';
		};
	
		this.setAge = function(age) {
			if(typeof age == "number") {
				this.age = age;
			} else {
				throw 'The type of the Age is not "Int". Current: ' + typeof age + 'Into a User object';
			}
		};
	
		this.setSkills = function(Skills) {
			if(typeof Skills == "object") {
				this.Skills = Skills;
			} else {
				throw 'The type of the Skills is not "Object". Current: ' + typeof Skills + 'Into a User object';
			}
		};
	
		this.setStatus = function(Status) {
			return Status;
		};
	
		this.setTimetable = function(Timetable) {
			return Timetable;
		};
	
		this.setCreations = function(Creations) {
			return Creations;
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setUsername(Attributes.username);
		this.setPassword(Attributes.password);
		this.setName(Attributes.name);
		this.setLastname(Attributes.lastname);
		this.setPicture(Attributes.picture);
		this.setAge(Attributes.age);
		this.setSkills(Attributes.Skills);
		this.setStatus(Attributes.Status);
		this.setTimetable(Attributes.Timetable);
		this.setCreations(Attributes.Creations);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getUsername = function() {
			return this.username;
		};
	
		this.getPassword = function() {
			return this.password;
		};
	
		this.getName = function() {
			return firstLetterInMaj(this.name);
		};
	
		this.getLastname = function() {
			return firstLetterInMaj(this.lastname);
		};
	
		this.getType = function() {
			return this.type;
		};
	
		this.getPicture = function() {
			return this.picture;
		};
	
		this.getAge = function() {
			return this.age;
		};
	
		this.getSkills = function() {
			return this.Skills;
		};
	
		this.getStatus = function() {
			return this.status;
		};
	
		this.getTimetable = function() {
			return this.Timetable;
		};
	
		this.getCreations = function() {
			return this.Creations;
		};
	
		this.getComputerNumber = function() {
			return this.computerNumber;
		};
	
		//Methods /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getLetteredStatus = function() {
			switch(this.status) {
				case 1:
					return 'Non-abonné';
					break;
				case 2:
					return 'Abonné';
					break;
				case 3:
					return 'Demi journée';
					break;
			}
		}
	
		this.listSkills = function(Skills) {
			var list = "";
			for (var i = 0; i < this.getSkills().length; i++) {
				list += Skills[i].getName() + ' : Niveau ' + Skills[i].getLevel() + '<br>';
			};
	
			return list;
		}
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class DiscourseSubject : //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.DiscourseSubject = function(Attributes) {
	
		/*
			id: 		Int 	(the ID of the subject in the database)
			title: 		String 	(title of the subject)
			content: 	String 	(The content of the subject, first message)
			User: 		Object 	(the User which have written the subject)
			category: 	String  (the Discourse's category of the subject)
			activity: 	String 	(last activity on the subject)
			Answers: 	Array 	(contains the Answers' information of the subject)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.title;
		this.User;
		this.content;
		this.category;
		this.activity;
		this.Answers;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a DiscrouseSubject object';
			}
		};
	
		this.setTitle = function(title) {
			this.title = title + '';
		};
	
		this.setContent = function(content) {
			this.content = content + '';
		};
	
		this.setUser = function(User) {
			if(typeof User == "object") {
				this.User = User;
			} else {
				throw 'The type of the User is not "Object". Current: ' + typeof User + '. Into a DiscourseSubject object';
			}
		};
	
		this.setCategory = function(category) {
			this.category = category + '';
		};
	
		this.setActivity = function(activity) {
			this.activity = activity + '';
		};
	
		this.setAnswers = function(Answers) {
			if(typeof Answers == "array") {
				this.Answers = Answers;
			} else {
				throw 'The type of the Answers is not "Array". Current: ' + typeof Answers + '. Into a DiscourseSubject object';
			}
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setTitle(Attributes.title);
		this.setUser(Attributes.User);
		this.setContent(Attributes.content);
		this.setCategory(Attributes.category);
		this.setActivity(Attributes.activity);
		this.setAnswers(Attributes.Answers);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getTitle = function() {
			return this.title;
		};
	
		this.getUser = function() {
			return this.User;
		};
	
		this.getCategory = function() {
			return this.category;
		};
	
		this.getActivity = function() {
			return this.activity;
		};
	
		this.getAnswers = function() {
			return this.Answers;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class DiscourseAnswer : //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.DiscourseAnswer = function(Attributes) {
	
		/*
			id: 		Int 	(the ID of the answer in the database)
			User: 		Object 	(the User which have written the subject)
			content: 	String 	(the text of the answer)
			activity: 	string 	(date of the answer)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.User;
		this.content;
		this.activity;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a DiscourseAnswer object';
			}
		};
	
		this.setUser = function(User) {
			if(typeof User == "object") {
				this.User = User;
			} else {
				throw 'The type of the User is not "Object". Current: ' + typeof User + '. Into a DiscourseAnswer object';
			}
		};
	
		this.setContent = function(content) {
			this.content = content + '';
		};
	
		this.setActivity = function(activity) {
			this.activity = activity + '';
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setUser(Attributes.User);
		this.setContent(Attributes.content);
		this.setActivity(Attributes.activity);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getUser = function() {
			return this.User;
		};
	
		this.getContent = function() {
			return this.content;
		};
	
		this.getActivity = function() {
			return this.activity;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class Machine : //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.Machine = function(Attributes) {
		/*
			id: 		Int 	(the ID of the Machine in the database)
			name: 		String 	(The name of the machine ex. 'Ultimaker-1")
			Usage: 		Object 	(Object containing the usage of the machine day after day)
			doc: 		String 	(A link to a PDF saying how to use the machine)
			Materials: 	Array 	(List of materials usable by the machine)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.name;
		this.Usage;
		this.doc;
		this.Materials;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a Machine object';
			}
		};
	
		this.setName = function(name) {
			this.name = name + '';
		};
	
		this.setUsage = function(Usage) {
			if(typeof Usage == "object") {
				this.Usage = Usage;
			} else {
				throw 'The type of the Usage is not "Object". Current: ' + typeof Usage + '. Into a Machine object';
			}
		};
	
		this.setDoc = function(doc) {
			this.doc = doc + '';
		};
	
		this.setMaterials = function(Materials) {
			if(typeof Materials == "object") {
				this.Materials = Materials;
			} else {
				throw 'The type of the Materials is not "Object". Current: ' + typeof Materials + '. Into a Machine object';
			}
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setName(Attributes.name);
		this.setUsage(Attributes.Usage);
		this.setDoc(Attributes.doc);
		this.setMaterials(Attributes.Materials);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getName = function() {
			return this.name;
		};
	
		this.getUsage = function() {
			return this.Usage;
		};
	
		this.getDoc = function() {
			return this.doc;
		};
	
		this.getMaterials = function() {
			return this.Materials;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class Material : /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.Material = function(Attributes) {
		/*
			id: 		Int 	(the ID of the Material in the database)
			name: 		String 	(The name of the Material ex. 'Dremell abrasive discs")
			Usage: 		Object 	(Object containing the usage of the Material day after day)
			doc: 		String 	(A link to a PDF saying how to use the Material)
			Unit:  		Object 	(An Object which contains singular and plural word of the unit to use with the Material ex: '
									{
										'singular': 'plate',
										'plural': 'plates'
									}
								') Why ? Because it's funny!
			remaining: 	Int 	(The number which describe how many units it remains ex: '2 plates')
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.name;
		this.Usage;
		this.doc;
		this.Unit;
		this.remaining;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a Material object';
			}
		};
	
		this.setName = function(name) {
			this.name = name + '';
		};
	
		this.setUsage = function(Usage) {
			if(typeof Usage == "object") {
				this.Usage = Usage;
			} else {
				throw 'The type of the Usage is not "Object". Current: ' + typeof Usage + '. Into a Material object';
			}
		};
	
		this.setDoc = function(doc) {
			this.doc = doc + '';
		};
	
		this.setUnit = function(Unit) {
			if(typeof Unit == "object") {
				this.Unit = Unit;
			} else {
				throw 'The type of the Unit is not "Object". Current: ' + typeof Unit + '. Into a Material object';
			}
		};
	
		this.setRemaining = function(remaining) {
			this.remaining = remaining + '';
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setName(Attributes.name);
		this.setUsage(Attributes.Usage);
		this.setDoc(Attributes.doc);
		this.setUnit(Attributes.Unit);
		this.setRemaining(Attributes.remaining);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getName = function() {
			return this.name;
		};
	
		this.getUsage = function() {
			return this.Usage;
		};
	
		this.getDoc = function() {
			return this.doc;
		};
	
		this.getUnit = function() {
			return this.Unit;
		};
	
		this.getRemaining = function() {
			return this.remaining;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class Skill : ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.Skill = function(Attributes) {
		/*
			id: 		Int 	(the ID of the Skill in the database)
			name: 		String 	(The name of the skill)
			description:String 	(A quick description of what the skill is about)
			level: 		Int 	(From 0 to 5)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.name;
		this.description;
		this.level;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a Skill object';
			}
		};
	
		this.setName = function(name) {
			this.name = name + '';
		};
	
		this.setDescription = function(description) {
			this.description = description + '';
		};
	
		this.setLevel = function(level) {
			if(typeof level == "number") {
				this.level = level;
			} else {
				throw 'The type of the level is not "Int". Current: ' + typeof level + 'Into a Skill object';
			}
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setName(Attributes.name);
		this.setDescription(Attributes.description);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getName = function() {
			return this.name;
		};
	
		this.getDescription = function() {
			return this.description;
		};
	
		this.getLevel = function() {
			return this.level;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class Type : ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.Type = function(Attributes) {
		/*
			id: 		Int 	(the ID of the Type in the database)
			name: 		String 	(The name of the Type)
			description:String 	(A quick description of what the Type is about)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.name;
		this.description;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a Type object';
			}
		};
	
		this.setName = function(name) {
			this.name = name + '';
		};
	
		this.setDescription = function(description) {
			this.description = description + '';
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setName(Attributes.name);
		this.setDescription(Attributes.description);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getName = function() {
			return this.name;
		};
	
		this.getDescription = function() {
			return this.description;
		};
	};
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// Class Status : ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	this.Status = function(Attributes) {
		/*
			id: 		Int 	(the ID of the Material in the database)
			name: 		String 	(The name of the skill)
			description:String 	(A quick description of what the skill is about)
			Permissiosn:Object 	(Object which contains the permnissions of the status)
		*/
	
		// Attributes /////////////////////////////////////////////////////////////
	
		this.id;
		this.name;
		this.description;
		this.permissions;
	
		//Setters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.setId = function(id) {
			if(typeof id == "number") {
				this.id = id;
			} else {
				throw 'The type of the ID is not "Int". Current: ' + typeof id + 'Into a Status object';
			}
		};
	
		this.setName = function(name) {
			this.name = name + '';
		};
	
		this.setDescription = function(description) {
			this.description = description + '';
		};
	
		this.setPermissions = function(permissions) {
			this.permissions = permissions + '';
		};
	
		//Setting attributes by setters functions
	
		this.setId(Attributes.id);
		this.setName(Attributes.name);
		this.setDescription(Attributes.description);
	
		//Getters /////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////
	
		this.getId = function() {
			return this.id;
		};
	
		this.getName = function() {
			return this.name;
		};
	
		this.getDescription = function() {
			return this.description;
		};
	
		this.getLevel = function() {
			return this.permissions;
		};
	};
};
